package com.epam;
/**
 * @autor Yan Bordiianu
 * @version 1.3
 * @since 1.1
 * This is my Main class, here I wil write my <string>code</string>
 */

import java.util.Scanner;

/**
 *
 */
public class Application {
    public static void main(String[] args) {
        Scanner keyboardInput = new Scanner(System.in);
        System.out.println("Insert smallest number: ");
        int mini;
        int maxi;
        int sumOdd = 0;
        int sumEven = 0;
        int sizeOfSet;
        int amountOfOddNumbers = 0;
        int amountOfEvenNumbers = 0;
        mini = keyboardInput.nextInt();
        System.out.println("Insert biggest number: ");
        maxi = keyboardInput.nextInt();

        for (int i = mini; i <= maxi; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                sumOdd = sumOdd + i;
                amountOfOddNumbers++;
            }
        }
        System.out.println();
//        System.out.println("Amount Of Odd Numbers: " + amountOfOddNumbers);
        for (int i = maxi; i >= mini; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
                sumEven = sumEven + i;
                amountOfEvenNumbers++;
            }
        }
        System.out.println();
//        System.out.println("Amount Of Even Numbers: " + amountOfEvenNumbers);
        System.out.println("Sum of odd numbers: " + sumOdd);
        System.out.println("Sum of even numbers: " + sumEven);

        System.out.println("Insert size of set Fibonacci: ");
        sizeOfSet = keyboardInput.nextInt();
        int n1;
        int n2;
        int n3;
        n1 = mini;
        n2 = mini + 1;
        int maxOddFib = 1;
        int maxEvenFib = 2;
        int amountOfEvenFibNumbers = 1;
        int amountOfOddFibNumbers = 1;
        System.out.print("Fibonacci interval: " + 1 + " " + n2);

        for (int i = 2; i < sizeOfSet; ++i) {
            n3 = n1 + n2;
            if (n3 <= maxi) {
                System.out.print(" " + n3);

                if (n3 % 2 != 0) {
                    maxOddFib = n3;
                    amountOfOddFibNumbers++;
                }
                if (n3 % 2 == 0) {
                    maxEvenFib = n3;
                    amountOfEvenFibNumbers++;
                }
                n1 = n2;
                n2 = n3;
            }
        }
        System.out.println();
//        System.out.println("amountOfOddFibNumbers: " + amountOfOddFibNumbers);
//        System.out.println("amountOfEvenFibNumbers: " + amountOfEvenFibNumbers);
        System.out.println("F1: " + maxOddFib);
        System.out.println("F2: " + maxEvenFib);

        float percentOddFib;
        float percentEvenFib;
        percentOddFib = (float) amountOfOddFibNumbers / (float) amountOfOddNumbers * 100;
        System.out.println("Persent of odd Fibonacci: " + percentOddFib+ "%");
        percentEvenFib = (float) amountOfEvenFibNumbers / (float) amountOfEvenNumbers * 100;
        System.out.println("Persent of even Fibonacci: " + percentEvenFib + "%");
    }
}